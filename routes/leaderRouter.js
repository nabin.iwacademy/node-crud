const express = require("express");
const bodyParser = require("body-parser");
const leaderRouter = express.Router();
const authenticate = require('../authenticate')
leaderRouter.use(bodyParser.json());
leaderRouter
    .route("/")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/plain");
        next();
    })
    .get((req, res, next) => {
        res.end("will send all data related to leader");
    })

    .post(authenticate.verifyUser, (req, res, next) => {
        res.end(
            "will send post data" +
            req.body.name +
            "with details :" +
            req.body.description
        );
    })
    .put(authenticate.verifyUser, (req, res, next) => {
        res.end("put operations not supported for / leader  ");
    })

    .delete(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end("DELETE operations supported for /promotions");
    })
leaderRouter.route("/:leaderId")
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader("Content-Type", "text/plain");
        next();
    })
    .get((req, res, next) => {
        res.end("will send data od dish" + req.params.leaderId + "to you");
    })

    .post(authenticate.verifyUser, (req, res, next) => {
        res.end("post operations not supported for / promotion " + req.params.leaderId);
    })

    .put(authenticate.verifyUser, (req, res, next) => {
        res.write("we will update the leader" + req.params.leaderId + "\n");
        res.end(
            "will update the dish" +
            req.body.name +
            "with details :" +
            req.body.description
        );
    })

    .delete(authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end("DELETE operations supported for" + req.params.leaderId);
    });


module.exports = leaderRouter;