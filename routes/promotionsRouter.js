const express = require("express");
const bodyParser = require("body-parser");
const promotionRouter = express.Router();
promotionRouter.use(bodyParser.json());
const Promotions = require('../models/promotions')
promotionRouter
    .route("/")
    .get((req, res, next) => {
        Promotions.find({})
            .then((promotions) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", 'application/json');
                res.json(promotions);
            }, (err) => next(err))
            .catch((err) => next(err))

    })

    .post((req, res, next) => {
        Promotions.create(req.body)
            .then((promotions) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json")
                res.json(promotions);
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .put((req, res, next) => {
        res.end("put operations not supported for / promotions  ");
    })

    .delete((req, res, next) => {
        Promotions.remove({})
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err))

    });

promotionRouter.route("/:promotionId")
    .get((req, res, next) => {
        Promotions.findById(req.params.promotionId)
            .then((promotions) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json")
                res.json(promotions);

            }, (err) => next(err))
            .catch((err) => next(err))
    })

    .post((req, res) => {
        res.statusCode = 403
        res.end("post operations not supported for / promotion " + req.params.promotionId);
    })

    .put((req, res, next) => {
        Promotions.findByIdAndUpdate(req.params.promotionId, {
                $set: req.body
            }, {
                new: true
            })
            .then((dish) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json")
                res.json(dish);
            }, (err) => next(err))
            .catch((err) => next(err))
    })

    .delete((req, res, next) => {
        Promotions.findByIdAndRemove(req.params.promotionId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader("Content-Type", "application/json");
                res.json(resp);
            }, (err) => next(err))
            .catch(err => next(err))

    });


module.exports = promotionRouter;